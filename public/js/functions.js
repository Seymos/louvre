function setFullHeight(item) {
    item.css('height',window.innerHeight);
}
function setFullWidth(item) {
    item.css('width',window.innerWidth);
}
function setFullScreen(item) {
    item.css('height',window.innerHeight);
    item.css('width',window.innerWidth);
}

// activation navigation principale en responsive
function navigationResponsive() {
    $("#btn_responsive").on('click', function () {
        if (panel_container.hasClass('active')) {
            console.log("Désactivé");
            panel_container.removeClass('active');
            ico_rsp.removeClass('fa-times');
            ico_rsp.addClass('fa-bars');
        } else {
            console.log("Activé");
            panel_container.addClass('active');
            ico_rsp.addClass('fa-times');
            ico_rsp.removeClass('fa-bars');
        }
    });
}

// activation navigation secondaire en non responsive
function secondNavigation() {
    btn2.on('click', function () {
        nav2.css('transition','all .25s');
        ico2.css('transition','all .25s');
        panel_body.css('transition','all .25s');
        if (nav2.hasClass('active')) {
            nav2.removeClass('active');
            ico2.addClass('fa-long-arrow-right');
            ico2.removeClass('fa-long-arrow-left');
            panel_body.css('display','block');
        } else {
            nav2.addClass('active');
            ico2.addClass('fa-long-arrow-left');
            ico2.removeClass('fa-long-arrow-right');
            panel_body.css('display','none');
        }
    });
}

function showPanier() {
    btn_panier.on('click', function () {
        console.log('panier');
        if (panier.hasClass('active')) {
            panier.removeClass('active');
        } else {
            panier.addClass('active');
        }
    });
}