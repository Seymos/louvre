<?php

namespace App\Services;

use App\Entity\Ordering;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use \Twig_Environment as Twig;

class Mailer {

	public $environment;
	private $mailer;

	/**
	 * Mailer constructor.
     * @param Twig $environment
     * @param \Swift_Mailer $mailer
	 */
	public function __construct( Twig $environment, \Swift_Mailer $mailer ) {
		$this->environment = $environment;
		$this->mailer     = $mailer;
	}

    /**
     * @param Ordering $ordering
     * @param SessionInterface $session
     * @return bool
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
	public function sendEmailConfirmation(Ordering $ordering, SessionInterface $session)
	{
		$tickets = $ordering->getTickets();
		$message = (
			new \Swift_Message("Votre réservation pour le musée du Louvre"))
			->setFrom("reservation@louvre.louisthomas.fr")
			->setTo($ordering->getEmail())
			->setBody(
				$this->environment->render(
					'emails/reservation_confirmed.html.twig',
					array(
						'order' => $ordering,
						'tickets' => $tickets,
						'total' => $session->get('total')
					)
				),
				'text/html'
			)
		;
		if ($this->mailer->send($message)) {
		    return true;
        } else {
		    return false;
        }
	}
}
