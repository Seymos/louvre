<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use \Stripe\Stripe as Stripes;
use \Stripe\Customer as Customer;
use \Stripe\Charge as Charge;

class Stripe {

    public function checkPayment(Request $request)
	{
	    $total = $request->getSession()->get('total');
        Stripes::setApiKey("sk_test_Ysvj1WNmrhVLLl4IUDohamsC");
        $stripeToken = $request->get('stripeToken');
        if (!isset($stripeToken)) {
            return new RedirectResponse('payment');
        }
        $token = $request->get('stripeToken');
        $customer = Customer::create(
            array(
                "source" => $token,
                "description" => "Example customer"
            )
        );
        $charge = Charge::create(
	        array(
	            "amount" => $total*100,
                "currency" => "eur",
                "customer" => $customer->id
            )
        );

        return $charge;
	}
}
