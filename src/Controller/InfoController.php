<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InfoController extends Controller
{
    /**
     * @Route("/mentions-legales", name="mentions_legales")
     */
    public function mentions_legales()
    {
        return $this->render(
            'info/mentions_legales.html.twig'
        );
    }
    /**
     * @Route("/conditions-generales-de-vente-et-utilisation", name="cgvu")
     */
    public function cgvu()
    {
        return $this->render(
            'info/cgvu.html.twig'
        );
    }
    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render(
            'info/contact.html.twig'
        );
    }
}
