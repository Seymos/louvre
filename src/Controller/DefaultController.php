<?php
namespace App\Controller;

use App\Entity\Ordering;
use App\Entity\Ticket;
use App\Form\OrderingType;
use App\Form\TicketType;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
	public function index(Request $request)
	{
        $session = $request->getSession();
        if ($session->has('order')) {
            $order = $session->get('order');
        } else {
            $order = new Ordering();
        }
		$form = $this->createForm(OrderingType::class, $order);
		$orders = $this->getDoctrine()->getManager()->getRepository(Ordering::class)->findToDay();
		$total_tickets = $this->get('louvre.calculator')->getNumberOfTickets($orders, $session);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
            $session->set('code_reservation',$order->getCode());
            $session->set('order', $order);
            return $this->redirectToRoute('informations');
		}
		return $this->render(
			'index.html.twig',
			array(
				'form' => $form->createView(),
				'tickets' => $total_tickets
			)
		);
	}

    /**
     * @Route("/informations", name="informations")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
	public function information(Request $request)
	{
		$session = $request->getSession();
		if (!$session->has('order')) {
			$this->addFlash('warning',"Veuillez remplir la réservation svp");
			return $this->redirectToRoute('index');
		}
		$order = $session->get('order');
        $number = $order->getNumber();
        if ($session->has('step2')) {
            $form = $this->createForm(CollectionType::class, $session->get('step2'), array('entry_type' => TicketType::class));
        } else {
            // création des tickets
            for ($i = 0; $i < $number; $i++) {
                $tickets[] = new Ticket();
            }
            $form = $this->createForm(CollectionType::class, $tickets, array('entry_type' => TicketType::class));
        }
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$a = 0;
			foreach ($form->getData() as $ticket) {
				$ticket->setPricingWith();
				$session->set('ticket'.$a,$ticket);
				$order->addTicket($ticket);
				$a++;
			}
			$session->set('order',$order);
			$session->set('step2',$form->getData());
			if (!$session->has('total')) {
				$session->set('total',0);
			}
			$this->container->get('louvre.calculator')->total($order->getNumber(), $session);
			$this->addFlash('success',"Merci pour vos informations, vous pouvez maintenant procéder au règlement de votre commande.");
			return $this->redirectToRoute('payment');
		}

		return $this->render(
			'informations.html.twig',
			array(
				'order' => $order,
				'form' => $form->createView(),
                'nb' => $number
			)
		);
	}

    /**
     * @Route("/payment", name="payment")
     * @param Request $request
     * @return Response
     */
	public function payment(Request $request) {
		$session = $request->getSession();
		if (!$session->has('step2') && !$session->has('order')) {
			$this->addFlash('warning',"Veuillez remplir la réservation");
			return $this->redirectToRoute('index');
		} elseif (!$session->has('step2') && $session->has('order')) {
			$this->addFlash('warning',"Veuillez remplir vos informations");
			return $this->redirectToRoute('informations');
		}
		$order = $session->get('order');
		for ($i = 0; $i < $order->getNumber(); $i++) {
			$tickets[] = $session->get( 'ticket' . $i );
		}
		$total = $session->get('total');

		if ($request->isMethod('POST')) {
		    // paiement
            $charge = $this->container->get('louvre.stripe')->checkPayment($request);
            $em = $this->getDoctrine()->getManager();
            if ($charge['captured'] === false) {
                $this->addFlash('danger',"Le paiement a échoué, merci de revoir vos informations de paiement.");
                return $this->redirectToRoute('payment');
            } else {
                $order->setPaied(true);
                $em->persist($order);
            }
            // send email
            $confirmation = $this->container->get('louvre.mailer')->sendEmailConfirmation($order, $session);
            if ($confirmation === true) {
                $order->setEmailSent(true);
                foreach ($tickets as $ticket) {
                    $em->persist($ticket);
                }
                $em->persist($order);
                $em->flush();
                $this->addFlash('success',"Le paiement a été validé avec succès, profitez de votre visite !.");
                return $this->redirectToRoute('payment_successful');
            } else {
                $em->persist($order);
                $em->flush();
                $this->addFlash('danger',"Le paiement a été validé mais vos billets n'ont pas été envoyés !.");
                return $this->redirectToRoute('payment_successful');
            }
		}

		return $this->render(
			'payment.html.twig',
			array(
				'order' => $order,
				'total' => $total,
				'tickets' => $tickets
			)
		);
	}

    /**
     * @Route("/payment-accepted", name="payment_successful")
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function paymentAccepted(SessionInterface $session) {
		$session->clear();
		return $this->render(
			'payment_accepted.html.twig'
		);
	}
}
