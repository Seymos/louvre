<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	private $lastname;

	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	private $firstname;

	/**
	 * @ORM\Column(type="string")
	 * @Assert\Country()
	 */
	private $country;

	/**
	 * @ORM\Column(type="date")
	 */
	private $birthdate;

	/**
	 * @ORM\Column(type="float")
	 */
	private $pricing;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $promotion;

    /**
     * @param null|string $lastname
     * @param string $firstname
     * @param string $country
     * @param \DateTime $birthdate
     * @param float $pricing
     * @param bool $promotion
     */
	public function hydrate(?string $lastname, string $firstname, string $country, \DateTime $birthdate, bool $promotion)
    {
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->country = $country;
        $this->birthdate = $birthdate;
        $this->promotion = $promotion;
    }

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * @param mixed $lastname
	 */
	public function setLastname( $lastname ) {
		$this->lastname = $lastname;
	}

	/**
	 * @return mixed
	 */
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * @param mixed $firstname
	 */
	public function setFirstname( $firstname ) {
		$this->firstname = $firstname;
	}

	/**
	 * @return mixed
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param mixed $country
	 */
	public function setCountry( $country ) {
		$this->country = $country;
	}

	/**
	 * @return mixed
	 */
	public function getBirthdate() {
		return $this->birthdate;
	}

	/**
	 * @param mixed $birthdate
	 */
	public function setBirthdate( $birthdate ) {
		$this->birthdate = $birthdate;
	}

	/**
	 * @return mixed
	 */
	public function getPricing() {
		return $this->pricing;
	}

	public function setPricing($pricing)
    {
        $this->pricing = $pricing;
    }

	public function setPricingWith() {
		if ($this->getPromotion() === true) {
			$this->pricing = 10;
		} else {
			// récupération date naissance
			$birthdate = $this->getBirthdate();
			// récupération date du jour
			$now = new \DateTime('now');
			// calcul de l'age de la personne
			$age  = date_diff($now, $birthdate, true)->y;
			// calcul du tarif
			$pricing = $this->getPricingWithAge($age);
			// attribution du tarif correspondant (swith?)

			// faire une fonction pour le tarif ?
			$this->pricing = $pricing;
		}
	}

	/**
	 * @return boolean
	 */
	public function getPromotion() {
		return $this->promotion;
	}

	/**
	 * @param mixed $tarif_reduit
	 */
	public function setPromotion( $promotion ) {
		$this->promotion = $promotion;
	}

	public function getPricingWithAge($age) {
		switch ($age) {
			case $age > 0 && $age < 4:
				$pricing = 0;
				break;
			case $age >= 4 && $age < 12:
				$pricing = 8;
				break;
			case $age >= 12 && $age < 60:
				$pricing = 16;
				break;
			default:
				$pricing = 12;
		}
		return $pricing;
	}
}
