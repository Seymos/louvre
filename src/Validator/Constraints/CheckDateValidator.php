<?php

namespace App\Validator\Constraints;


use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CheckDateValidator extends ConstraintValidator {

	private $session;

	/**
	 * CheckDateValidator constructor.
	 *
	 * @param $session
	 */
	public function __construct( SessionInterface $session ) {
		$this->session = $session;
	}


	public function validate( $value, Constraint $constraint ) {
        $type = $this->session->get('pre_submit')['type'];
        $day = $this->session->get('pre_submit')['day'];
		$date = new \DateTime('now');
		$current_date = $date->format('d/m/Y');
		$current_time = $date->format('H:i');
		if ($day === $current_date && $type === "J") {
			if ($current_time > '14:00') {
                $this->context->addViolation("Vous ne pouvez plus commander de billet Journée pour aujourd'hui.");
            }
		}
	}
}
